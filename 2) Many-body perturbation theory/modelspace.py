import numpy as np

SPECNOTSTRING = 'spdfghijklmnopqrstuv'

class modelspace():
    def __init__(self, eMax):
        print("Building modelspace with eMax = %i" % eMax)

        self.eMax = eMax

        self.J2max = 2 * (eMax + 1)

        self.e = []
        self.n = []
        self.l = []
        self.twoj = []
        self.twom = []
        self.t = []
        self.nlj_s = []

        self.specnot = []

        s = 0
        nlj = 0
        for e in range(eMax+1):
            lMin = e % 2
            for l in range(lMin, e + 1, 2):
                n = (e - l) / 2;

                twojMin = abs(2 * l - 1)
                twojMax = 2 * l + 1

                for twoj in range(twojMin,twojMax+2,2):
                    for twom in range(-twoj,twoj+2,2):
                        for t in range(0,1+1):
                            s += 1

                            self.e.append(e)
                            self.n.append(n)
                            self.t.append(t)
                            self.l.append(l)
                            self.twoj.append(twoj)
                            self.twom.append(twom)
                            self.nlj_s.append(nlj)

                            if t==0:
                                specnot = "n-" + str(int(n)) + SPECNOTSTRING[l] + "_" + str(twoj) + "/2"
                            else:
                                specnot = "p-" + str(int(n)) + SPECNOTSTRING[l] + "_" + str(twoj) + "/2"

                            self.specnot.append(specnot)

                    nlj += 1

        self.sMax = s - 1

        print("Obtained N=%i " % (self.sMax + 1) , "basis functions")

        return

    def print_modelspace(self):
        for s in range(self.sMax+1):
            print("%i:" % s, " with %s" % self.specnot[s])

        return
    
    def symmetry_overview(self):
        print("Check symmetries of operator: \n")
        nonzero_pi = 0
        nonzero_tz = 0
        nonzero_twom = 0

        dim = self.sMax+1

        for p in range(dim):
            for q in range(dim):
                if self.t[p] == self.t[q]:
                    nonzero_tz += 1

                if self.l[p]%2 == self.l[q]%2:
                    nonzero_pi += 1

                if self.twom[p] == self.twom[q]:
                    nonzero_twom += 1

        tot = dim**2
        print("    Percentage of nonzeros based on 'Isospin' (1B):          %2.2f %%" % float(100. * nonzero_tz / tot))
        print("    Percentage of nonzeros based on 'Parity' (1B):           %2.2f %%" % float(100. * nonzero_pi / tot))
        print("    Percentage of nonzeros based on 'AM Projection (1B)':    %2.2f %%" % float(100. * nonzero_twom / tot))
        print("")

        nonzero_pi = 0
        nonzero_tz = 0
        nonzero_twom = 0
        for p in range(dim):
            for q in range(dim):
                for r in range(dim):
                    for s in range(dim):
                        if self.t[p] + self.t[q] == self.t[r] + self.t[s]:
                            nonzero_tz += 1

                        if (self.l[p] + self.l[q]) % 2 == (self.l[r] + self.l[s]) % 2:
                            nonzero_pi += 1

                        if (self.twom[p] + self.twom[q]) == (self.twom[r] + self.twom[s]) :
                            nonzero_twom += 1

        tot = dim ** 4
        print("    Percentage of nonzeros based on 'Isospin' (2B):          %2.2f %%" % float(100. * nonzero_tz / tot))
        print("    Percentage of nonzeros based on 'Parity' (2B):           %2.2f %%" % float(100. * nonzero_pi / tot))
        print("    Percentage of nonzeros based on 'AM Projection (2B)':    %2.2f %%" % float(100. * nonzero_twom / tot))
        print("")