from sys import argv

import hartreefock
import matrixelements
import mbpt
import modelspace
import parameters
import utility
import tensor
import profiler

import numpy as np
import pywigxjpf as wig

import math
import time

np.set_printoptions(precision=8, linewidth=300, suppress=None)

def main():
    utility.header_message()

    ################################################################################################################

    utility.section_message("Runtime parameters")

    params = parameters.parameters()

    ################################################################################################################

    utility.section_message("Modelspace construction")

    ms = modelspace.modelspace(params.eMax)
    ms_read = modelspace.modelspace(params.eMax_read)

    ms.symmetry_overview()

    ################################################################################################################

    utility.section_message("Angular-momentum cache")

    t1 = time.time()
    wig.wig_table_init(10, 9)
    wig.wig_temp_init(10)
    t2 = time.time()
    profiler.add_timing("Wigner Cache", t2 - t1)

    ################################################################################################################

    utility.section_message("Formation of HO tensor")

    meid = 'N3LO_EMN500_srg1.8'
    ham = matrixelements.build_hamiltonian(ms, ms_read, params, meid)

    ################################################################################################################

    utility.section_message("Generate initial HO density")

    rho = tensor.generate_density(ms,params)

    ################################################################################################################

    utility.section_message("Hartree-Fock theory")

    hf = hartreefock.hf(ms,ham)

    hf.set_initial_density(rho)

    hf.iterate()

    holes, particles = hf.get_holes_particles()

    ################################################################################################################

    utility.section_message("Normal ordering")

    ham.normal_order(hf.rho)

    ################################################################################################################

    utility.section_message("Basis transformation")

    ham_hf = ham.transform_basis(ms,hf.coeff)

    ################################################################################################################

    utility.section_message("Many-body perturbation theory")

    pt = mbpt.mbpt(ms)

    pt.init_denominator(holes, particles, ham_hf)

    pt.calc_e2(ham_hf)
    pt.calc_e3(ham_hf)

    ################################################################################################################

    utility.section_message("Timings")

    profiler.print_timings()

    ################################################################################################################

    utility.footer_message()

if __name__ == "__main__":
    main()