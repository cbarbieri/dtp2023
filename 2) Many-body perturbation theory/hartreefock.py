import numpy as np
import modelspace
import profiler
import tensor
import time

np.set_printoptions(precision=8, linewidth=300, suppress=None)

class hf():
    def __init__(self, modelspace, ham):
        print("Initialize Hartree-Fock class")

        self.iter = 0
        self.iterMax = 100

        self.prec = 1e-6

        self.ms = modelspace

        self.dim = modelspace.sMax + 1

        self.is_converged = False

        self.ham = ham

        self.EHF = 0.

        self.eps = np.zeros(self.dim)
        self.occs = np.zeros(self.dim)
        self.coeff = np.zeros((self.dim, self.dim))

        self.rho = np.zeros((self.dim, self.dim))
        self.h = np.zeros((self.dim,self.dim))

    def iterate(self):
        diff = 100.

        alpha = 0.6
        print("Iterating HF equations [prec = %2.6f, alpha_mixing = %2.2f]" % (self.prec,alpha), "\n")

        self.calc_expectation_value()

        print("Starting energy E = % 2.6f" % self.EHF, "\n")

        while diff > self.prec:
            self.iter += 1
            rho_old = self.rho

            self.calc_field()

            self.diagonalize()

            rho_new = self.update_density()

            # mixing of density
            self.rho = alpha * rho_new + (1. - alpha) * rho_old

            self.calc_expectation_value()

            diff = np.linalg.norm(self.rho - rho_old,"fro")

            # ceck if required precision goal was reached
            if diff < self.prec:
                print("\nHF equations converged up to precision of % 2.6f " % self.prec)
                self.is_converged = True
                break

            print("     ",
                "Iteration: %2i" % self.iter,
                "||Delta rho|| = %2.6f" % diff,
                "EHF = % 2.6f " % self.EHF,
                "tr(rho) = % 2.6f " % self.A
            )

            if self.iter > self.iterMax:
                print("\nMaximum number of iteration reached ... exiting!\n")
                exit()

        if self.is_converged:
            self.calc_expectation_value()

        return

    def set_initial_density(self, rho):
        self.rho = rho

        self.occs = np.diag(self.rho)

        if tensor.check_symmetry_1b(self.rho) == False:
            print("Initial density not symmetric ... exiting!")
            exit()

        A = np.einsum('ii->', self.rho)
        print("Initial tr(rho) = %2.6f " % A)

    def init_density(self, parameters, modelspace):
        print("Initialize starting density")

        t1 = time.time()
        N = 0
        Z = 0
        for s in range(modelspace.sMax+1):
            t = modelspace.t[s]

            if t == 0 and N < parameters.N:
                self.rho[s, s] = 1.
                N += 1
            elif t == 1 and Z < parameters.Z:
                self.rho[s, s] = 1.
                Z += 1

        for s in range(modelspace.sMax + 1):
            self.occs[s] = self.rho[s, s]

        if tensor.check_symmetry_1b(self.rho) == False:
            print("Initial density not symmetric ... exiting!")
            exit()

        A = np.einsum('ii->', self.rho)
        t2 = time.time()
        profiler.add_timing("HF: Init density", t2 - t1)
        print("Initial tr(rho) = %2.6f " % A)

    def calc_expectation_value(self):
        t1 = time.time()
        E_1B = np.einsum('pq,pq->', self.ham.O_1B, self.rho)
        E_2B = 0.5 * np.einsum('pqrs,pr,qs->',self.ham.O_2B, self.rho, self.rho)
        self.EHF = E_1B + E_2B
        t2 = time.time()

        if self.is_converged:
            print("")
            print("      1B part: % f " % E_1B)
            print("      2B part: % f " % E_2B)
            print("      Total:   % f " % self.EHF)
            print("")

        profiler.add_timing("HF: Expectation value", t2 - t1)

    def diagonalize(self):
        # t1 = time.time()
        # self.eps, self.coeff = np.linalg.eigh(self.h)
        # t2 = time.time()

        t1 = time.time()
        twomMax = max(self.ms.twom)
        twojMax = max(self.ms.twoj)
        coeff_loc_all = np.zeros((self.dim,self.dim))
        for t in {0, 1}:
            for pi in {0, 1}:
                for twoj in range(1,twojMax+1,2):
                    for twom in range(-twomMax,twomMax+1,2):
                        indices = []
                        for p in range(self.ms.sMax+1):
                            if self.ms.t[p] == t and self.ms.l[p]%2 == pi and self.ms.twoj[p] == twoj and self.ms.twom[p] == twom:
                                indices.append(p)

                        if indices == []:
                            continue

                        np.asarray(indices)

                        h_loc = self.h[np.ix_(indices,indices)]

                        eps_loc, coeffs_loc = np.linalg.eigh(h_loc)

                        self.eps[np.ix_(indices)] = eps_loc
                        coeff_loc_all[np.ix_(indices,indices)] = coeffs_loc

        self.coeff = coeff_loc_all
        t2 = time.time()

        profiler.add_timing("HF: Diagonalization", t2 - t1)

    def update_density(self):
        t1 = time.time()
        rho_new = np.einsum('pr,r,qr->pq', self.coeff, self.occs, self.coeff)

        self.A = np.einsum('ii->', self.rho)

        if tensor.check_symmetry_1b(self.rho) == False:
            print("one-body density field not symmetric ... exiting!")
            exit()

        t2 = time.time()
        profiler.add_timing("HF: Density update", t2 - t1)

        return rho_new

    def calc_field(self):
        t1 = time.time()
        h_loc = np.zeros_like(self.h)
        h_loc += np.einsum('pq->pq', self.ham.O_1B)
        h_loc += np.einsum('prqs,rs->pq', self.ham.O_2B, self.rho)

        self.h = h_loc

        if tensor.check_symmetry_1b(self.h) == False:
            print("HF field not symmetric ... exiting!")
            exit()

        t2 = time.time()
        profiler.add_timing("HF: Calculate field", t2 - t1)

    def get_holes_particles(self):
        holes = []
        particles = []
        for p in range(self.dim):
            if abs(self.occs[p]) > 1e-4:
                holes.append(p)
            else:
                particles.append(p)

        return holes, particles
