import numpy as np

INVM = 20.7355285386

class parameters():
    def __init__(self):
        print("Initialize global paramater class")

        self.N = 2
        self.Z = 2

        self.hwHO = 20
        self.aHO = np.sqrt(2.0 * INVM / self.hwHO)
        self.eMax = 1
        self.eMax_read = 2

        self.A = self.N + self.Z

        return
