import numpy as np
import gzip
import tensor
from tqdm import tqdm
import profiler
import time

np.set_printoptions(precision=8, linewidth=300, suppress=None)

import pywigxjpf as wig

INVM = 20.7355285386

def get_cg(twoj1, twoj2, twoj3, twom1, twom2, twom3):
    if twom1 + twom2 != twom3:
        return 0.

    return pow(-1,(-twoj1 + twoj2 - twom3)/2) * np.sqrt(twoj3 + 1.) * wig.wig3jj(twoj1,twoj2,twoj3,twom1,twom2,-twom3)


def read_matrixelements(modelspace, modelspace_read, filename):
    print("Reading input file: '%s' " %  filename)

    dim = modelspace.sMax + 1
    dim_read = modelspace_read.sMax + 1

    op = np.zeros((dim,dim,dim,dim))

    # local HO indices
    e_nlj = []
    n_nlj = []
    l_nlj = []
    twoj_nlj = []

    nlj = 0
    for e in range(modelspace_read.eMax + 1):
        lMin = e % 2
        for l in range(lMin, e+1, 2):
            n = (e - l) / 2;

            twojMin = abs(2 * l - 1)
            twojMax = 2 * l + 1

            for twoj in range(twojMin, twojMax + 2, 2):
                e_nlj.append(e)
                n_nlj.append(n)
                l_nlj.append(l)
                twoj_nlj.append(twoj)

                nlj += 1

    nljMax = nlj - 1

    iMax = 0
    for nlj1 in range(nljMax + 1):
        for nlj2 in range(nlj1 + 1):
            for nlj3 in range(nlj1 + 1):

                nlj4Max = 0
                if nlj3 == nlj1:
                    nlj4Max = nlj2
                else:
                    nlj4Max = nlj3

                for nlj4 in range(nlj4Max + 1):

                    # parity constraint
                    if (l_nlj[nlj1] + l_nlj[nlj2])%2 != (l_nlj[nlj3] + l_nlj[nlj4])%2:
                        continue

                    Jmin = int(max(
                        (abs(twoj_nlj[nlj1] - twoj_nlj[nlj2]) / 2),
                        (abs(twoj_nlj[nlj3] - twoj_nlj[nlj4]) / 2)
                    ))

                    Jmax = int(min(
                        (twoj_nlj[nlj1] + twoj_nlj[nlj2]) / 2,
                        (twoj_nlj[nlj3] + twoj_nlj[nlj4]) / 2
                    ))

                    if Jmin > Jmax:
                        continue

                    for J in range(Jmin,Jmax+1):
                        for T in range(0,2):
                            for MT in range(-T,T+1):
                                iMax += 1

    ME = np.zeros(iMax+1)

    l = -1
    i = 0
    with open(filename) as file:
        for line in file:
            l += 1
            # skip first row
            if l == 0:
                continue

            mels = list(line.split())
            for c in range(len(mels)):
                ME[i] = mels[c]
                i += 1

    Jmax = 2 * modelspace_read.eMax + 2
    mels_JT = np.zeros((nljMax+1,nljMax+1,nljMax+1,nljMax+1,Jmax,2,3))

    i = -1
    for nlj1 in range(nljMax + 1):
        twoj1 = twoj_nlj[nlj1]
        for nlj2 in range(nlj1 + 1):
            twoj2 = twoj_nlj[nlj2]
            for nlj3 in range(nlj1 + 1):

                nlj4Max = 0
                if nlj3 == nlj1:
                    nlj4Max = nlj2
                else:
                    nlj4Max = nlj3

                for nlj4 in range(nlj4Max + 1):

                    # parity constraint
                    if (l_nlj[nlj1] + l_nlj[nlj2])%2 != (l_nlj[nlj3] + l_nlj[nlj4])%2:
                        continue

                    twoj3 = twoj_nlj[nlj3]
                    twoj4 = twoj_nlj[nlj4]

                    Jmin = int(max(
                        (abs(twoj1 - twoj2) / 2),
                        (abs(twoj3 - twoj4) / 2)
                    ))

                    Jmax = int(min(
                        (twoj1 + twoj2) / 2,
                        (twoj3 + twoj4) / 2
                    ))

                    if Jmin > Jmax:
                        continue

                    for J in range(Jmin,Jmax+1):
                        for T in range(0,2):
                            for MT in range(-T,T+1):
                                # i += 4 * (J - Jmin) + 2 * T + MT
                                i += 1

                                me = ME[i]

                                if abs(me) < 1e-6:
                                    continue

                                # if abs(me) > 1e-4:
                                #     print(nlj1,nlj2,nlj3,nlj4,J,T,MT,me)

                                mels_JT[nlj1][nlj2][nlj3][nlj4][J][T][MT + 1] = me
                                mels_JT[nlj2][nlj1][nlj3][nlj4][J][T][MT + 1] = pow(-1,(J + T - (twoj1 + twoj2) / 2)) * me
                                mels_JT[nlj1][nlj2][nlj4][nlj3][J][T][MT + 1] = pow(-1,(J + T - (twoj3 + twoj4) / 2)) * me
                                mels_JT[nlj2][nlj1][nlj4][nlj3][J][T][MT + 1] = pow(-1,(2*J + 2*T - (twoj1 + twoj2 + twoj3 + twoj4) / 2)) * me

                                mels_JT[nlj3][nlj4][nlj1][nlj2][J][T][MT + 1] = me
                                mels_JT[nlj3][nlj4][nlj2][nlj1][J][T][MT + 1] = pow(-1, (J + T - (twoj1 + twoj2) / 2)) * me
                                mels_JT[nlj4][nlj3][nlj1][nlj2][J][T][MT + 1] = pow(-1, (J + T - (twoj3 + twoj4) / 2)) * me
                                mels_JT[nlj4][nlj3][nlj2][nlj1][J][T][MT + 1] = pow(-1, (2 * J + 2 * T - (twoj1 + twoj2 + twoj3 + twoj4) / 2)) * me

    print("Generating m-scheme 4-index tensor 'o[p][q][r][s]' \n")

    for p in tqdm(range(modelspace.sMax+1)):
        twojp = modelspace.twoj[p]
        twomp = modelspace.twom[p]
        nlj1 = modelspace.nlj_s[p]
        t1 = modelspace.t[p]
        for q in range(p):
            twojq = modelspace.twoj[q]
            twomq = modelspace.twom[q]
            nlj2 = modelspace.nlj_s[q]
            t2 = modelspace.t[q]
            for r in range(modelspace.sMax+1):
                twojr = modelspace.twoj[r]
                twomr = modelspace.twom[r]
                t3 = modelspace.t[r]
                nlj3 = modelspace.nlj_s[r]

                sMax = 0
                if p == r:
                    sMax = q
                else:
                    sMax = r

                # for s in range(sMax):
                for s in range(r):
                # for s in range(modelspace.sMax + 1):
                    twojs = modelspace.twoj[s]
                    twoms = modelspace.twom[s]
                    t4 = modelspace.t[s]
                    nlj4 = modelspace.nlj_s[s]

                    if t1 + t2 != t3 + t4:
                        continue

                    if (modelspace.l[p] + modelspace.l[q] - modelspace.l[r] + modelspace.l[s]) % 2 != 0:
                        continue

                    if twomp + twomq != twomr + twoms:
                        continue

                    Jmin = int(max(
                        (abs(twojp - twojq) / 2),
                        (abs(twojr - twojs) / 2)
                    ))

                    Jmax = int(min(
                        (twojp + twojq) / 2,
                        (twojr + twojs) / 2
                    ))

                    if Jmin > Jmax:
                        continue

                    MT = int((2 * t1 - 1 + 2 * t2 - 1) / 2)

                    me = 0.
                    for J in range(Jmin,Jmax+1):
                        cg0 = get_cg(twojp,twojq,2*J,twomp,twomq,twomp+twomq) * get_cg(twojr,twojs,2*J,twomr,twoms,twomr+twoms)
                        if abs(cg0) < 1e-4:
                            continue

                        # T = 0
                        if MT == 0:
                            if t1 != t3:
                                cg = -0.5 * cg0
                            else:
                                cg = +0.5 * cg0

                            me += cg * mels_JT[nlj1][nlj2][nlj3][nlj4][J][0][MT+1]

                        # T = 1
                        if MT == 0:
                            cg = 0.5 * cg0
                        else:
                            cg = cg0

                        me += cg * mels_JT[nlj1][nlj2][nlj3][nlj4][J][1][MT+1]

                    if abs(me) < 1e-6:
                        continue

                    op[p][q][r][s] = + me
                    op[q][p][r][s] = - me
                    op[p][q][s][r] = - me
                    op[q][p][s][r] = + me

                    op[r][s][p][q] = + me
                    op[r][s][q][p] = - me
                    op[s][r][p][q] = - me
                    op[s][r][q][p] = + me

    # if tensor.check_symmetry_2b(op) == False:
    #     print("Symmetries of input operator violated ... exiting!\n")
    #     exit()

    print("")

    return op


def generate_kinetic_energy(modelspace, params):
    print("Generating one-body matrix elements of kinetic energy operator")
    op = np.zeros((modelspace.sMax+1,modelspace.sMax+1))
    for s1 in range(modelspace.sMax+1):
        for s2 in range(modelspace.sMax + 1):

            if modelspace.l[s1]%2 != modelspace.l[s2]%2:
                continue

            if modelspace.t[s1] != modelspace.t[s2]:
                continue

            if modelspace.twoj[s1] != modelspace.twoj[s2]:
                continue

            if modelspace.twom[s1] != modelspace.twom[s2]:
                continue

            n1 = modelspace.n[s1]
            n2 = modelspace.n[s2]

            l1 = modelspace.l[s1]

            me = 0.
            if n1 == n2:
                me += (1.0 - 1.0 / params.A) * INVM / (params.aHO * params.aHO) * (2 * n1 + l1 + 1.5)
            elif  n1 == n2 - 1:
                me += (1.0 - 1.0 / params.A) * INVM / (params.aHO * params.aHO) * np.sqrt((n1 + 1.0) * (n1 + l1 + 1.5))
            elif n1 == n2 + 1:
                me += (1.0 - 1.0 / params.A) * INVM / (params.aHO * params.aHO) * np.sqrt((n2 + 1.0) * (n2 + l1 + 1.5))
            else:
                continue

            op[s1, s2] = me

    if tensor.check_symmetry_1b(op) == False:
        print("One-body part of Hamiltonian is not symmetric ... exiting!\n")
        exit()

    return op

def build_hamiltonian(modelspace, modelspace_read, parameters, meid):
    t1 = time.time()
    ham = tensor.tensor(modelspace)

    ham.O_0B = 0.
    ham.O_1B = generate_kinetic_energy(modelspace, parameters)

    filename = 'input/tpp_eMax0' + str(parameters.eMax_read) + '.me2j'
    tpp = read_matrixelements(modelspace, modelspace_read, filename)

    filename = 'input/' + meid + '_eMax0' + str(parameters.eMax_read) + '_hwHO0' + str(parameters.hwHO) + '.me2j'
    vnn = read_matrixelements(modelspace, modelspace_read, filename)

    # put this in separate function
    ham.O_2B = 2. / parameters.A * 1./pow(parameters.aHO,2) * tpp + vnn

    t2 = time.time()

    profiler.add_timing("Hamiltonian formation", t2 - t1)

    return ham