# layout when executing code
printwidthleftmargin = 10
printwidth = 100

headerwidth = printwidth + printwidthleftmargin
headersymbol = "x"
########################################################################################################################

author = " 'your name here! ' "
affiliation = " 'your affiliation here! "

def header_message():
    print("\n\n")
    print(headersymbol * headerwidth)
    print(headersymbol)
    print(headersymbol + " Many-body solver for chiral Hamiltonian")
    print(headersymbol + " written by " + author)
    print(headersymbol + " Affiliation:" + affiliation)
    print(headersymbol)
    print(headersymbol * headerwidth)

def footer_message():
    section_message("CODE TERMINATED SUCCESFULLY")


def section_message(x):
    print("\n" + "*"*(printwidthleftmargin) + " " + x + " " + "*"*(printwidth-len(x)) + "\n")