import numpy as np

import profiler
import time
import tensor

class mbpt():
    def __init__(self, ms):
        print("Initialize MBPT class")

        dim = ms.sMax+1

        self.den_singles = np.zeros((dim, dim))
        self.den_doubles = np.zeros((dim, dim, dim, dim))

        return

    def init_denominator(self, holes, particles, ham):
        print("Initialize MBPT denominators")

        for a in particles:
            for i in holes:
                self.den_singles[a, i] = 1. / (ham.O_1B[i,i] - ham.O_1B[a,a])

        for a in particles:
            for b in particles:
                for i in holes:
                    for j in holes:
                        self.den_doubles[a,b,i,j] = 1. / (ham.O_1B[i,i] + ham.O_1B[j,j] - ham.O_1B[a,a] - ham.O_1B[b,b])

    def calc_e2(self, ham):
        print("Calculating second-order energy correction")

        t1 = time.time()
        e2_1B = np.einsum('ai,ai,ai->', ham.O_1B, self.den_singles, ham.O_1B, optimize="optimal")
        t2 = time.time()

        profiler.add_timing("E2 [non canonical]",t2-t1)

        t1 = time.time()
        e2_2B = 0.25 * np.einsum('abij,abij,abij->', ham.O_2B, self.den_doubles, ham.O_2B, optimize="optimal")
        # e2_2B = 0.25 * np.einsum('abij,abij->', ham.O_2B, ham.O_2B, optimize="optimal")
        t2 = time.time()

        profiler.add_timing("E2 [canonical]", t2 - t1)

        print("")
        print("    E2 [non-canonical] = % 2.6f" % e2_1B)
        print("    E2 [    canonical] = % 2.6f" % e2_2B)
        print("    E2 [        total] = % 2.6f" % (e2_1B + e2_2B) )
        print("")

        return

    def calc_e3(self, ham):
        print("Calculating third-order energy correction")

        t1 = time.time()
        e3_hh = 1./8. * np.einsum('abij,abij,ijkl,abkl,abkl->', ham.O_2B, self.den_doubles, ham.O_2B, ham.O_2B, self.den_doubles, optimize="optimal")
        t2 = time.time()

        profiler.add_timing("E3 [hh channel]",t2-t1)

        t1 = time.time()
        e3_pp = 1./8. * np.einsum('abij,abij,abcd,cdij,cdij->', ham.O_2B, self.den_doubles, ham.O_2B, ham.O_2B, self.den_doubles, optimize="optimal")
        t2 = time.time()
        profiler.add_timing("E3 [pp channel]", t2 - t1)

        t1 = time.time()
        e3_ph = - np.einsum('abij,abij,kbic,ackj,ackj->', ham.O_2B, self.den_doubles, ham.O_2B, ham.O_2B, self.den_doubles, optimize="optimal")
        t2 = time.time()
        profiler.add_timing("E3 [ph channel]", t2 - t1)

        print("")
        print("    E3 [pp channel] = % 2.6f" % e3_pp)
        print("    E3 [hh channel] = % 2.6f" % e3_hh)
        print("    E3 [ph-channel] = % 2.6f" % e3_ph)
        print("    E3 [total]      = % 2.6f" % (e3_pp + e3_hh + e3_ph))
        print("")

        return