# ECT2023
## Many-body Perturbation Theory 

In this subpart of the course we will study the simplest case of a correlation expansion, i.e., 
many-body perturbation theory (MBPT). To this end we will extensively discuss a set of theretical
concepts that are important when designing correlation expansions.
The course will encompass a reminder on second quantization. We will then in detail
discuss the concept of normal ordering and Wick's theorem. Afterwards we will study
symmetry-conserving Hartree-Fock theory as the simplest case of a mean-field approach.
The design of a personal HF solver based on chiral interactions will be key for the 
subsequent treatment of low-order MBPT corrections.

Finally, we will spent time on introducing MBPT as a standalone technique for closed-shell nuclei.
Participants will write their own implementation of second- and third-order energy corrections
based on HF reference states.
The final two lectures are dedicated to modern MBPT developments, e.g., in the form of being an
auxiliary tool to support non-perturbative many-body simulations via basis optimization (natural orbitals) 
or pre-processing techniques (importance truncation). The set of lectures is concluded by 
introducing open-shell developments using symmetry-broken and multi-configurational reference 
states that are essential to cope with the strong static correlations present in atomic nuclei 
away from shell closures.

## Implementational details

#### Parameters
Runtime parameters can be altered in parameter.py module.
This concerns for example the use of different target proton/neutron number and, therefore,
other target nuclei. By default O16 is the target nucleus (N=8, Z=8). 
Switch N, Z to other values will automatically adapt the values in the treatment of the kinetic energy
in matrixelements.py. All calculations are restricted to eMax=2, i.e., 40 single-particle states.

#### Matrix elements
The code is shipped with a set of chiral two-body (NN) matrix elements in a highly compressed
hence not human-readable format (see '*.me2j' in /input). The participant should not worry about the details of the corresponding
implementation (matrixelement.py) - even though it can addressed during the lecture week if wanted.

#### Clebsch-Gordan coefficients
In addition the code requires the use Clebsch-Gordan coefficients. Due to performance considerations these
are provided through the package 'fastwigxjpf' developed by Johansson and Forssén.
While available through the python package index (pip),
```bash
pip install pywigxjpf
``` 
the installation has caused issues in some cases.
In that case the package must be downloaded and installed from source.

#### Profiler
The code comes with a very simple dictionary-based timing infrastructure contained in profiler.py.
It can be used via
```bash
    t1 = time.time()
    # do some stuff here
    t2 = time.time()
    profiler.add_timing("Your section tag", t2 - t1)
```
At all times the current profiler information can be accessed via
```bash
profiler.print_timings()
```
which provides an overview of the different timings in the various subsections.

#### Execution

Finally the code is executed via
```bash
python3 main.py
```

### Acknowledgements

I greatfully acknowledge the help of P. Arthuis, M. Heinz, T. Miyagi and A. Scalesi for testing the code suite.
 

