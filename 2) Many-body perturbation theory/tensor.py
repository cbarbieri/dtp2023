import numpy as np
import time
import tensor
import profiler

from tqdm import tqdm

class tensor():
    def __init__(self,modelspace):
        print("Initializing HO tensor class \n")
        dim = modelspace.sMax+1
        self.O_0B = 0.
        self.O_1B = np.zeros((dim,dim))
        self.O_2B = np.zeros((dim,dim,dim,dim))

        self.is_normal_ordered = False

        self.dim = dim

        return

    def normal_order(self, rho):
        if self.is_normal_ordered == True:
            print("Operator is already normal ordered ... breaking function!")
            return

        print("Normal ordering of HO operator \n")

        # normal order zero-body part
        t1 = time.time()
        self.O_0B += np.einsum('pq,pq->', self.O_1B, rho)
        self.O_0B += 0.5 * np.einsum('pqrs,pr,qs->', self.O_2B, rho, rho)
        t2 = time.time()

        print("    Zero-body part: %f \n\n" %self.O_0B)

        profiler.add_timing("Normal ordering [0B part]", t2 - t1)

        # normal order one-body part
        t1 = time.time()
        self.O_1B += np.einsum('pqrs,qs->pr', self.O_2B, rho)
        t2 = time.time()

        profiler.add_timing("Normal ordering [1B part]", t2 - t1)

        self.is_normal_ordered = True

    def undo_normal_order(self, rho):
        if self.is_normal_ordered == False:
            print("Operator is not normal ordered ... cannot undo normal ordering!")
            return

        print("Undo normal ordering of HO operator \n")

        # normal order one-body part
        t1 = time.time()
        self.O_1B -= np.einsum('pqrs,qs->pr', self.O_2B, rho)
        t2 = time.time()

        profiler.add_timing("Undo normal ordering [1B part]", t2 - t1)

        # normal order zero-body part
        t1 = time.time()
        self.O_0B -= np.einsum('pq,pq->', self.O_1B, rho)
        self.O_0B -= 0.5 * np.einsum('pqrs,pr,qs->', self.O_2B, rho, rho)
        t2 = time.time()

        print("    Zero-body part: %f \n\n" % self.O_0B)

        profiler.add_timing("Undo normal ordering [0B part]", t2 - t1)

        self.is_normal_ordered = False

    def transform_basis(self, ms, coeffs):
        print("Performing basis transformation")

        ham_new = tensor(ms)

        ham_new.is_normal_ordered = self.is_normal_ordered
        ham_new.dim = self.dim

        t1 = time.time()
        ham_new.O_1B = np.einsum('kl,kp,lq->pq', self.O_1B, coeffs, coeffs, optimize="optimal")
        t2 = time.time()

        profiler.add_timing("Basis transformation [1B part]", t2 - t1)

        t1 = time.time()
        ham_new.O_2B = np.einsum('klmn,kp,lq,mr,ns->pqrs', self.O_2B, coeffs, coeffs, coeffs, coeffs, optimize="optimal")
        t2 = time.time()

        profiler.add_timing("Basis transformation [2B part]", t2 - t1)

        if check_symmetry_2b(ham_new.O_2B) == False:
            print("Symmetry check after basis transformation violated ... exiting!")
            exit()

        return ham_new


def check_symmetry_1b(op, rtol=1e-05, atol=1e-05):
    op_21 = np.einsum('pq->qp',op)
    check =  np.allclose(op, op_21, rtol=rtol, atol=atol)
    if check == False:
        return check
    else:
        return True

def check_symmetry_2b(op, rtol=1e-05, atol=1e-08):
    op_2134 = -1. * np.einsum('pqrs->qprs',op)
    check_2134 = np.allclose(op, op_2134, rtol=rtol, atol=atol)

    op_1243 = -1. * np.einsum('pqrs->pqsr', op)
    check_1243 = np.allclose(op, op_1243, rtol=rtol, atol=atol)

    if check_1243 == False or check_2134 == False:
        return False
    else:
        return True

def generate_density(ms, params):
    print("Generating reference state density with N=%i and Z=%i \n" % (params.N, params.Z) )

    NN = 0
    ZZ = 0

    dim = ms.sMax+1
    rho = np.zeros((dim,dim))

    for s in range(ms.sMax+1):
        t = ms.t[s]

        # filling proton orbits
        if t == 0 and NN < params.N:
            NN += 1
            rho[s][s] = 1.

        # filling proton orbits
        if t == 1 and ZZ < params.Z:
            ZZ += 1
            rho[s][s] = 1.

    trace = np.einsum('pp->', rho)
    if trace != params.A:
        print("Trace of density %f is not equal to particle number %f ... exiting! \n" % (trace, params.A))
        exit()

    return rho