# DTP2023


The 2023 ECT* Doctoral Training Programme edition focuses on ab initio nuclear theory, with emphasis on modern computational methods and emerging technologies. The past decade has seen considerable progress in this field, leading to fully fledged computations with three-nucleon forces in medium mass isotopes. High-performance computing is now pivotal to the quest for reaching predictions of complex and heavy isotopes. Future frontiers will exploit Machine Learning and Quantum Computing algorithms as tools for many-body nuclear physics.

The aim of the DTP 2023 is to provide the participants with a pedagogical introduction to many-body theories that allow a deep understanding of nuclear structure, present the open challenges in relation to modelling nuclear reactions and interaction with weak probes, and providing know how for implementation with high-performance and GPU computing.  By the end of the program, the participants are expected to have a thorough understanding of current challenges in nuclear structure, have the background knowledge to grasp novel opportunities in the field, and should be equipped with the numerical tools to make their own contributions in tackling open problems. 

#  **__Student seminars sessions__**

As per the timetable, will hold students seminars on the afternoons of Wed. 19th and 26th. Talks will be approx. 7 minutes each but will give ereyone the opportunity to tell about her/his onw research project and interests. The can be abut a pojrct just started or some concluded work.

Please, fill up the [spreadsheet here](https://docs.google.com/spreadsheets/d/1VryzaOK6P8qH_BsCPKfm8VrFVbIPa8_icefWPHydw74/edit?usp=sharing) indicating which day you prefer to talk and an title of your contributions. We will make the final time table based on your suggestions. 

Please upload the slides for your talk in the followng folders
- [talks for Jul 19th](https://drive.google.com/drive/folders/1Sz0auEy93Yjqc6JVZO6rV3RN7vNc67_5)
- [talks for Jul 26th](https://drive.google.com/drive/folders/1TD-32kmCyW_NlsI7XQ1018_fPi42Osph)

## Information and preparation for the school

You will find organizational matters below here, in this file, and material to prepare before arriving to Trento in the ['Preliminary material' folder](https://gitlab.com/cbarbieri/dtp2023/-/tree/main/0\)%20Preliminary%20material). Please read these carefully!


## Time table

The [time table](https://gitlab.com/cbarbieri/dtp2023/-/tree/main/DTP2023_schedule_18July23.pdf) is in the main folder. It might be updated along the way, check here for the latest.

Morning sessions will be 9:30-11:00 and 11:30-13:00, afternoons are 15:00 to 18:00.

Be at the ECT* by 9:00 on day one!


## Getting started

The material in this repository can be cloned to your computer using one of the two following commands:

```bash
$ git clone https://gitlab.com/cbarbieri/dtp2023.git
```
or
```bash
$ git clone git@gitlab.com:cbarbieri/dtp2023.git
```

Once this is done, if new matrial is posted the local copy of the repository can be adjourned with a pull request as follows
```bash
$ cd dtp2023
$ git pull
```
It is good practice to **rename the files you modify** to avoid them being overwritten by the next pull.


## Computing

Hands-on comuting sessions will be based on the topics taught in the mornings. You should bring your own laptop, with the latest version of Python and the following libraries installed:

```
numpy >=1.24.2
tqdm >= 4.36.0
pywigxjpf >= 1.10
emcee>=3.1.4
matplotlib>=3.7.1
prettyplease>=0.11.2
pyDOE>=0.3.8
scipy>=1.11.1
jax
```

whenever we'll need intensive simulations it will be possible to connect to a temporary accounts on the local cluster of the Physics Department of Trento.
