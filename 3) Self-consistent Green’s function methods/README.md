
# ECT* Doctoral Training Programme 2023

## Self-consistent Green's function methods

### Lectures

This course will address in detail one of the expansion methods developed to tackle the nuclear many-body problem, namely the Self-consistent Green's function (SCGF) approach.
This technique is nowadays standardly used in ab initio nuclear structure theory to compute ground-state as well as excited-state properties of light and medium-mass nuclei, of both closed- and open-shell character.
 
Building on the concepts introduced in Alexander Tichai's lectures during the first week of the DTP, the course will cover the following topics

1) **Introduction**: a brief reminder of the ab initio nuclear many-body problem.

2) **Basics of Green's function theory**: the basic elements and properties of many-body Green's function theory, such as the spectral representation and Galitskii-Migdal-Koltun sum rule, will be introduced.

3) **Dyson equation**: the central equation in the SCGF method will be derived starting from the perturbative expansion of the one-body Green's function and with the help of Feynman diagrams.

4) **Approximation schemes**: most common approximations to the solution of the Dyson equation will be introduced. In particular a systematic approach dubbed Algebraic Diagrammatic Construction (ADC) will be examined. The possibility of rewriting Dyson equation as an eigenvalue problem, which has important practical consequences, will be also discussed.

5) **Numerical implementation**: different steps and techniques necessary to implement Dyson equation in numerical codes will be studied (Krylov projection, Importance truncation, Optimised reference state, ...).

6) **Gorkov formalism**: a framework to generalise Dyson theory to _symmetry-breaking_ reference states and thus applicable to open-shell nuclei will be introduced.

7) **Overview of recent applications**: some recent examples of state-of-the-art applications to nuclear structure will be reviewed.

### Programming sessions

Three exercise sessions will complement the theoretical course. They will have as main objective the writing of a Python code that solves Dyson equation in the second-order ADC scheme. The sessions will be tentatively devoted to, respectively

1) Implementation of basic objects (propagator, self-energy) and equations (e.g., Galitskii-Migdal-Koltun sum rule).

2) Construction and diagonalisation of Dyson equation in the ADC(2) scheme.

3) Optimisation: implementation of Krylov projection, importance truncation, optimised reference state.

Optional developments (e.g., Gorkov framework or optical potentials) will be discussed if time allows.

The starting point for the SCGF routines is the Hartree-Fock Python code implemented during the first week of the DTP. No other libraries are in principle needed.

### Acknowledgments

A substantial part of the Python code that served as a basis for the computational sessions was developed by Alberto Scalesi (CEA Saclay), who is gratefully acknowledged.




