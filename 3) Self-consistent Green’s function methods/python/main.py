from sys import argv

from dyson_matrix import dyson_matrix
import hartreefock
import matrixelements
import modelspace
import parameters
import utility
import scgf
import tensor
import profiler

import numpy as np
import pywigxjpf as wig

import math
import time

np.set_printoptions(precision=8, linewidth=300, suppress=None)

def main():
    utility.header_message()

    ################################################################################################################

    utility.section_message("Runtime parameters")

    params = parameters.parameters()

    ################################################################################################################

    utility.section_message("Modelspace construction")

    ms = modelspace.modelspace(params.eMax)

    ms.symmetry_overview()

    ################################################################################################################

    utility.section_message("Angular-momentum cache")

    t1 = time.time()
    wig.wig_table_init(10, 9)
    wig.wig_temp_init(10)
    t2 = time.time()
    profiler.add_timing("Wigner Cache", t2 - t1)

    ################################################################################################################

    utility.section_message("Formation of HO tensor")

    meid = 'N3LO_EMN500_srg1.8'
    ham = matrixelements.build_hamiltonian(ms, params, meid)

    ################################################################################################################

    utility.section_message("Generate initial HO density")

    rho = tensor.generate_density(ms,params)

    ################################################################################################################

    #utility.section_message("Normal ordering")

    #ham.normal_order(rho)

    ################################################################################################################

    utility.section_message("Hartree-Fock solver")

    hf = hartreefock.hf(ms, ham)

    hf.init_density(params, ms)

    hf.iterate()

    holes, particles = hf.get_holes_particles()

    ################################################################################################################

    utility.section_message("Self consistent Green's function")

    utility.subsection_message("ADC(1)")

    prop_hf = scgf.create_hf(ms, hf)

    scgf.GMK_sumrule(prop_hf, ham)

    utility.subsection_message("ADC(2)")

    dm = dyson_matrix(prop_hf)

    #dm.update_Sigma_static(ham)

    #dm.update_Sigma_dynamic(ham)

    #dm.build_matrix()

    #c, e, h, p = dm.diagonalise()

    #prop_ADC2 = scgf.create_dressed(ms, h, p, e, c)

    #scgf.GMK_sumrule(prop_ADC2, ham)

    ################################################################################################################

    utility.section_message("Timings")

    profiler.print_timings()

    ################################################################################################################

    utility.footer_message()

if __name__ == "__main__":
    main()
