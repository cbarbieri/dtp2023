import numpy as np
import tensor as op
import modelspace
import time
import utility
import profiler
import time

class dyson_matrix():

  def __init__(self, sp_prop):

    print("Initialise Dyson matrix")
    t1 = time.time()

    self.sp_prop = sp_prop

    self.Nh = sp_prop.en_holes.size
    self.Np = sp_prop.en_parts.size
    self.ms_dim = sp_prop.dim

    self.M = np.zeros((self.Np*self.Np*self.Nh, self.ms_dim))
    self.N = np.zeros((self.ms_dim, self.Nh*self.Nh*self.Np))
    self.E_l = np.zeros((self.Np*self.Np*self.Nh, self.Np*self.Np*self.Nh))
    self.E_s = np.zeros((self.Np*self.Nh*self.Nh, self.Np*self.Nh*self.Nh))
    self.hf = np.zeros((self.ms_dim, self.ms_dim))

    self.zerom = np.zeros((self.Np*self.Nh*self.Nh, self.Np*self.Np*self.Nh))

    self.dim_side = self.ms_dim + self.Nh*self.Nh*self.Np + self.Nh*self.Np*self.Np
    self.DM = np.zeros((self.dim_side, self.dim_side))

    print(" Linear dimension: ", self.dim_side)

    t2 = time.time()
    profiler.add_timing("SCGF: Initialise Dyson matrix", t2 - t1)


  def calc_dim(self, A, ms_dim):

    holes = A
    parts = ms_dim - A
    if parts < 0:
      print("You cannot have that many particles in this tiny model space...")
      exit()

    dim_side = ms_dim + holes*holes*parts + holes*parts*parts
    self.dim_side = dim_side
    print("Dyson matrix has linear dimension: ", dim_side)


  def update_M(self, ham):

    M_temp = 1./np.sqrt(2) * np.einsum('rm,sn,tl,mnal->rsta', self.sp_prop.X, self.sp_prop.X, self.sp_prop.Y, ham.O_2B, optimize='optimal')
    self.M = np.reshape(M_temp, (self.Np*self.Np*self.Nh, self.ms_dim))


  def update_N(self, ham):

    N_temp = 1./np.sqrt(2) * np.einsum('almn,rm,sn,tl->arst', ham.O_2B, self.sp_prop.Y, self.sp_prop.Y, self.sp_prop.X, optimize='optimal')
    self.N = np.reshape(N_temp, (self.ms_dim, self.Nh*self.Nh*self.Np))


  def update_E_l(self):

    vec = np.zeros(self.Np*self.Np*self.Nh)
    count = 0
    for i in self.sp_prop.en_parts:
      for j in self.sp_prop.en_parts:
        for k in self.sp_prop.en_holes:
          vec[count] = i+j-k
          count=count+1
    self.E_l.fill(0)
    np.fill_diagonal(self.E_l, vec)


  def update_E_s(self):

    vec = np.zeros(self.Nh*self.Nh*self.Np)
    count = 0
    for i in self.sp_prop.en_holes:
      for j in self.sp_prop.en_holes:
        for k in self.sp_prop.en_parts:
          vec[count] = i+j-k
          count=count+1
    self.E_s.fill(0)
    np.fill_diagonal(self.E_s, vec)


  def update_Sigma_static(self, ham):

    print("")
    print("Compute static self-energy")

    t1 = time.time()

    self.hf = np.einsum('pq->pq', ham.O_1B, optimize='optimal') + np.einsum('prqs,rs->pq', ham.O_2B, self.sp_prop.rho, optimize='optimal')
    print("Dimensions of sub-matrices")
    print(" HF  : ", self.hf.shape)

    t2 = time.time()
    profiler.add_timing("SCGF: Compute static self-energy", t2 - t1)



  def update_Sigma_dynamic(self, ham):

    print("")
    print("Compute dynamic self-energy")

    t1 = time.time()

    self.update_E_l()
    print("Dimensions of sub-matrices")
    print(" E_> : ", self.E_l.shape)

    self.update_E_s()
    print(" E_< : ", self.E_s.shape)

    self.update_M(ham)
    print(" M   : ", self.M.shape)

    self.update_N(ham)
    print(" N   : ", self.N.shape)

    t2 = time.time()
    profiler.add_timing("SCGF: Compute dynamic self-energy", t2 - t1)


  def build_matrix(self):

    print("")
    print("Build Dyson matrix")

    t1 = time.time()

    self.DM = np.block([
                        [self.hf,             self.M.transpose(),  self.N                ],
                        [self.M,              self.E_l,            self.zerom.transpose()],
                        [self.N.transpose(),  self.zerom,          self.E_s              ]
                                                                                         ])
    t2 = time.time()
    profiler.add_timing("SCGF: Build Dyson matrix", t2 - t1)


  def diagonalise(self):

    print("")
    print("Diagonalise Dyson matrix")

    t1 = time.time()

    E, coeff = np.linalg.eigh(self.DM)
    coeff = coeff.transpose()

    dim_DM = self.Np*self.Np*self.Nh + self.Nh*self.Nh*self.Np + self.ms_dim
    coeff = coeff[0:dim_DM, 0:self.ms_dim]

    print("")
    print("Separate holes and particles")

    par = 0.
    ind = 0
    for index, item in enumerate(coeff):
      par = par + sum(map(lambda x: x * x, item))
      ind = index
      if par > 16:
        break

    print(" Particle number: ", par)
    print("")

    Y = coeff[0:index+1, 0:self.ms_dim]

    X = coeff[index+1:dim_DM, 0:self.ms_dim]

    h = [*range(0, index+1, 1)]
    p = [*range(index+1, E.size, 1)]

    t2 = time.time()
    profiler.add_timing("SCGF: Diagonalise Dyson matrix", t2 - t1)

    return coeff, E, h, p

