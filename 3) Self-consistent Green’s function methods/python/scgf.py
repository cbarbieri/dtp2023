import numpy as np
import tensor as op
import modelspace
import utility
import matplotlib.pyplot as plt
import profiler
import time

class sp_prop():

  def __init__(self, modelspace, holes, particles, eps, Z):

    self.ms = modelspace
    self.dim = modelspace.sMax + 1

    self.holes = holes
    self.parts = particles

    self.eps = eps
    self.en_holes = np.array([self.eps[i] for i in holes])
    self.en_parts = np.array([self.eps[i] for i in particles])

    self.dim_holes = np.size(holes)
    self.dim_parts = np.size(particles)

    self.X = np.array([Z[i] for i in particles])
    self.Y = np.array([Z[i] for i in holes])
    self.rho = np.zeros((self.dim, self.dim))

    self.sf_holes = np.zeros(np.shape(self.en_holes))
    self.sf_parts = np.zeros(np.shape(self.en_parts))


  def print_info(self):

    print(" Total number of poles: ", self.dim_holes + self.dim_parts)
    print(" Holes                : ", self.dim_holes)
    print(" Particles            : ", self.dim_parts)
    print("")
    #print("Energies (holes):\n", self.en_holes)
    #print("")
    #print("Spectroscopic factors (holes):\n", self.sf_holes)
    #print("")
    #print("Energies (particles):\n", self.en_parts)
    #print("")
    #print("Spectroscopic factors (particles):\n", self.sf_parts)
    #print("")


  def calc_sf(self):

    for index, item in enumerate(self.Y):
      self.sf_holes[index] = sum(map(lambda i: i * i, item))
    for index, item in enumerate(self.X):
      self.sf_parts[index] = sum(map(lambda i: i * i, item))


  def build_rho(self):
    self.rho = np.matmul(self.Y.transpose(), self.Y)


  def plot_sf(self, string):

    en = np.concatenate((self.en_holes, self.en_parts))
    sf = np.concatenate((self.sf_holes, self.sf_parts))

    sep = (self.en_holes[-1] + self.en_parts[0]) / 2.

    plt.plot([sep,sep], [0,1], linestyle='dashed', color="purple")
    #plt.yscale('log')
    plt.bar(en, sf, align='center')

    plt.xlabel(r"$\varepsilon_i$ [MeV]")
    plt.ylabel("$SF_i$")
    plt.savefig(string + ".png", dpi=300, bbox_inches='tight')
    plt.close()

#######################


def create_hf(ms, hf):

  print("Build Hartree-Fock propagator")
  print("")

  t1 = time.time()

  holes, particles = hf.get_holes_particles()
  prop = sp_prop(ms, holes, particles, hf.eps, hf.coeff.transpose())

  prop.calc_sf()
  prop.build_rho()
  prop.print_info()

  t2 = time.time()
  profiler.add_timing("SCGF: Build ADC(1) propagator", t2 - t1)

  prop.plot_sf("sp_prop_HF")

  return prop


def create_dressed(ms, h, p ,e, c):

  print("Build ADC(2) propagator")
  print("")

  t1 = time.time()

  prop = sp_prop(ms, h, p, e, c)

  prop.calc_sf()
  prop.build_rho()
  prop.print_info()
  prop.plot_sf("sp_prop_ADC2")

  t2 = time.time()
  profiler.add_timing("SCGF: Build ADC(2) propagator", t2 - t1)

  return prop


def GMK_sumrule(prop, ham):

  print("Compute g.s. energy via GMK sum rule")
  print("")
  t1 = time.time()

  E_gmk1 = 0.5 * np.einsum('pq,qp->', ham.O_1B, prop.rho, optimize='optimal')
  E_gmk2 = 0.5 * np.einsum('k,ak,ka->', prop.en_holes, prop.Y.transpose(), prop.Y, optimize='optimal')
  E = E_gmk1 + E_gmk2

  print(" 1B part: % f " % E_gmk1)
  print(" 2B part: % f " % E_gmk2)
  print(" Total:   % f " % E)
  print("")

  t2 = time.time()
  profiler.add_timing("SCGF: GMK sum rule", t2 - t1)

