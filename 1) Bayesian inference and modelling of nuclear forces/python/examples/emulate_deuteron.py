import sys
sys.path.append('./../lib')

import numpy as np
import nn_studio as nn_studio
import chiral_potential as chiral_potential
from scipy import linalg
from scipy.special import spherical_jn as jn
import matplotlib.pyplot as plt
from matplotlib.ticker import MaxNLocator
import lec_values as lec_values
import pyDOE as pyDOE
import auxiliary as aux

np.random.seed(123)

# initialize an object for computing T-matrices, phase shifts,
N_largespace = 100
nn = nn_studio.nn_studio(jmin=0,jmax=1,tzmin=0,tzmax=0,Np=N_largespace,mesh_type='gauleg_finite')

# initialize an object for the chiral interaction (isospin symmetric LO, NLO in WPC available)
potential = chiral_potential.two_nucleon_potential('NLO',Lambda=500.0)

# give the potential to the nn-analyzer
nn.V = potential

# give the LECS to the potential (via the nn-analyzer)
input_lecs = lec_values.nlo_lecs
nn.lecs = input_lecs.copy()

_,deuteron_channel = nn.lookup_channel_idx(l=0,ll=2,s=1,j=1)
_,mu = nn.lab2rel(0,0)

N = 2*(nn.Np)
ww = np.hstack((nn.wmesh,nn.wmesh))
pp = np.hstack((nn.pmesh,nn.pmesh))

#define the list of lecs to emulate the response for
lecs = ['C_3S1','D_3S1','D_3S1-3D1']
nlecs = len(lecs)

#compute kinetic energy
T = np.zeros((N,N))
for i, p_bra in enumerate(pp):
    T[i][i] = p_bra**2/(2*mu)

#zero all lecs that we are analyzing and compute constant part of V, i.e., V_0
for key in lecs:
    nn.lecs[key] = 0.0

V_0 = np.zeros((N,N))
this_V = nn.setup_Vmtx(deuteron_channel[0])[0]
for i, p_bra in enumerate(pp):
    for j, p_ket in enumerate(pp):
        V_0[i][j] = this_V[i][j]*p_bra*p_ket*np.sqrt(ww[i]*ww[j])

#prepare list for holding all 'split' elements of the Hamiltonian (H_0 + sum_i H_i), where H_0 = T+V_0 
Hs_largespace = []

Hs_largespace.append(T+V_0)
#loop over potential setting lecs to 1, one-by-one
V = np.zeros((N,N))
for lec in lecs:
    #set lec to 1.0
    print(f'splitting: {lec}')
    nn.lecs[lec] = 1.0
    this_V = nn.setup_Vmtx(deuteron_channel[0])[0]
    V = np.zeros((N,N))
    for i, p_bra in enumerate(pp):
        for j, p_ket in enumerate(pp):
            V[i][j] = this_V[i][j]*p_bra*p_ket*np.sqrt(ww[i]*ww[j])
    V -= V_0
    Hs_largespace.append(V)
    #set lec back to 0
    nn.lecs[lec] = 0.0

# setup a latin hypercube design for creating N snapshots
N_snapshots = 15

ndim = nlecs
npts = N_snapshots
snapshot_points = pyDOE.lhs(ndim,npts)

lim_lo = [None]*ndim
lim_hi = [None]*ndim

lim_lo[0] = -0.5 ; lim_hi[0] = +0.5
lim_lo[1] = -2.0 ; lim_hi[1] = +2.0
lim_lo[2] = -2.0 ; lim_hi[2] = +2.0

for j in range(npts):
    for i in range(ndim):
        snapshot_points[j, i] = aux.scale_point(snapshot_points[j, i], lim_lo[i], lim_hi[i])

fig = plt.figure(1)
ax = fig.add_subplot(111, projection='3d')
ax.scatter(snapshot_points[:,0],snapshot_points[:,1],snapshot_points[:,2],color='orange',edgecolor='black',s=30,marker='o')
ax.set_xlim(lim_lo[0],lim_hi[0])
ax.set_ylim(lim_lo[1],lim_hi[1])
ax.set_zlim(lim_lo[2],lim_hi[2])
ax.set_xlabel(r'$C_{3S1}$',fontsize=16)
ax.set_ylabel(r'$D_{3S1}$',fontsize=16)
ax.set_zlabel(r'$D_{3S1-3D1}$',fontsize=16)
plt.savefig(f'em_lhs_{N_snapshots}_snaps.pdf')


#we need a function to assemble the Hamiltonian from a list of Hamiltonian terms
# pnt = array of lec values that multiply the Hamiltonian terms [] (not including the 1.0 multiplying the constant)
# function tailored to f(x) = x linearity
def assemble_hamiltonian(Hs,pnt):
    
    H = 0.0
    H += 1.0*Hs[0]
    for idx,pnt_i in enumerate(pnt):
        H += pnt_i*Hs[idx+1]
    return H

# solve (generalized, if norm is not None) eigenvalue problem and return eigval & eigvec for lowest eigval
def solve_eigenvalue_problem(mtx,norm=None):

    eigvals, eigvecs = linalg.eigh(mtx,norm)
    s = np.argsort(eigvals)
    eigval = eigvals[s[0]]
    eigvec = eigvecs[:,s[0]]

    return eigval, eigvec
    
#prepare list to store snapshots
snapshots = []

for idx, this_point in enumerate(snapshot_points):
    H = assemble_hamiltonian(Hs_largespace,this_point)
    eigval, eigvec = solve_eigenvalue_problem(H)
    print(f'snapshot {idx}: E = {eigval}')
    snapshots.append(eigvec)
    
#prepare list for holding all 'split' elements of the Hamiltonian in the subspace
Hs_subspace = []

# projector = N_largespace x N_snapshots
print(f'constructing {N_largespace}x{N_snapshots} projector')
projector = np.array(snapshots).T

#svd on projector optimize the basis using SVD: this picks the n left
# singular vectors corresponding to (normalized) singular values below
# some 'threshold'. The singular vectors are orthogonal and yields an
# identity norm and we don't have to solve a generalized eigenvalue
# problem (numerically easier and more stable)
SVD_optimization = False
if SVD_optimization:
    threshold = 1e-4
    print()
    print(f'optimizing basis using SVD [threshold = {threshold}]')
    print()
    U_svd, S_svd, V_svd = np.linalg.svd(projector)
    #normalize the singular values (descending order by numpy algorithm)
    S_svd = S_svd/S_svd[0]
    print(f'S_svd = {S_svd/S_svd[0]}')

    n_singular_values_below_threshold = (S_svd>threshold).sum()
    print(f'retaining the first {n_singular_values_below_threshold} left singular vectors')

    #selecting the left singular vectors corresponding to the n_values_below_threshold vectors
    # also ordered descending by numpy algorithm
    print(f'constructing new {N_largespace}x{n_singular_values_below_threshold} projector')
    projector = U_svd[:,0:n_singular_values_below_threshold]
    
# project to subspace
# norm
norm = projector.T@projector

#check the norm determinant (unless you used a singular value decomposition to
#further reduce the projector, in which case the norm=1)
if np.allclose(norm,np.eye(norm.shape[0])):
    norm=None
else:
    #analyze the norm matrix
    print(f'det(norm) = {np.linalg.det(norm)}')
    
# the Hamiltonian terms
for this_H_largespace in Hs_largespace:
    this_H_subspace = projector.T@this_H_largespace@projector
    Hs_subspace.append(this_H_subspace)
    
# diagonalizing at snapshots return the largespace eigenvalues exactly
for idx, this_point in enumerate(snapshot_points):
    H = assemble_hamiltonian(Hs_subspace,this_point)
    eigval, eigvec = solve_eigenvalue_problem(H,norm)
    print(f'[subspace] snapshot {idx}: E = {eigval}')
    
#use the known lec_values as a first test point
test_lecs = np.array([lec_values.nlo_lecs[key] for key in lec_values.nlo_lecs if key in lecs])

#diagonalize in large space
H = assemble_hamiltonian(Hs_largespace,test_lecs)
eigval, eigvec = solve_eigenvalue_problem(H)
print(f'[large-space] test: E = {eigval}')

#diagonalize in subspace
H = assemble_hamiltonian(Hs_subspace,test_lecs)
eigval, eigvec = solve_eigenvalue_problem(H,norm)
print(f'[subspace] test: E = {eigval}')

# run for a few more test points to inspect the performance of the emulator

N_test = 100

ndim = nlecs
npts = N_test
test_points = pyDOE.lhs(ndim,npts)

lim_lo = [None]*ndim
lim_hi = [None]*ndim

lim_lo[0] = -0.5 ; lim_hi[0] = +0.5
lim_lo[1] = -2.0 ; lim_hi[1] = +2.0
lim_lo[2] = -2.0 ; lim_hi[2] = +2.0

for j in range(npts):
    for i in range(ndim):
        test_points[j, i] = aux.scale_point(test_points[j, i], lim_lo[i], lim_hi[i])

sim_E = []
emu_E = []
        
for test_point in test_points:
    print(f'test point = {test_point}')

    #diagonalize in large space
    H = assemble_hamiltonian(Hs_largespace,test_point)
    simulator_eigval, _ = solve_eigenvalue_problem(H)
    print(f'[large-space] test: E = {simulator_eigval}')
    sim_E.append(simulator_eigval)
    
    #diagonalize in subspace
    H = assemble_hamiltonian(Hs_subspace,test_point)
    emulator_eigval, _ = solve_eigenvalue_problem(H,norm)
    print(f'[subspace] test: E = {emulator_eigval}')
    emu_E.append(emulator_eigval)

fig = plt.figure(2)
ax = fig.add_subplot(111)
ax.scatter(sim_E,emu_E,color='red',edgecolor='black',alpha=0.4,s=35)
xy = np.linspace(*ax.get_xlim())
ax.plot(xy, xy, color='k')
ax.set_xlabel('simulator (MeV)')
ax.set_ylabel('emulator (MeV)')
plt.savefig(f'sim_vs_emu_{N_snapshots}_snaps.pdf')

fig = plt.figure(3)
ax = fig.add_subplot(111)
ax.scatter(np.arange(0,N_test,1),np.array(sim_E)-np.array(emu_E),color='red',edgecolor='black',alpha=0.4,s=35)
ax.set_xlabel('index')
ax.set_ylabel('simulator - emulator (MeV)')
plt.tight_layout()
plt.savefig(f'energy_residuals_{N_snapshots}_snaps.pdf')

