# ECT DTP 2023

I'm looking forward to working together on 'Bayesian inference and modelling of nuclear forces'

## To prepare

To prepare for this module of the course you can brush up on angular-momentum coupling and scattering theory. If you can, have a look at Chapters 3.5 (angular momentum) and 6.1 (scattering) in J.J. Sakurai: "Modern Quantum Mechanics" (second edition).
To run the Python codes it is useful to have basic knowledge of Python: functions, classes, lists, arrays, loops, plotting. There is plenty of material available online, e.g., https://sites.engineering.ucsb.edu/~shell/che210d/python.pdf

Have a look in the file ./python/requirements.txt to ensure that you have a Python (3) environment that can run the codes for this course module. 

## Bayesian inference and modelling of nuclear forces

- This module of the course consists of 4 lectures. The aim of the lecture series is to enable you to construct your own interactions and quantify uncertainties in a realistic setting such that you can take this further and perform your own research at the forefront of ab initio nuclear theory.

- During the lectures we will study the technical aspects of constructing a quantitative nuclear interaction (2-body only). This entails some discussion about chiral effective field theory up to next-to-leading order (in Weinberg power counting), the theory of nucleon-nucleon scattering and the partial-wave expansion. As a prototypical ab initio calculation we will solve the Schrödinger equation for the deuteron and predict its binding energy and radius. In the second half of the course we will take a closer look at how to infer the values of the low-energy constants (LECs) that parametrize the nucleon-nucleon interaction. To that end we will cover the foundations of Bayesian inference and parameter estimation. Towards the end we will make contact with methods for emulating nucleon-nucleon scattering amplitudes and the deuteron energy and wavefunction. This will help speed up the evaluation of the Bayesian posteriors that quantify your uncertainty in the LECs and ab initio predictions you make.

- The lectures are accompanied by self-contained Python code for analyzing the two-nucleon system for positive (scattering) and negative (binding) energies.

- There are two exercises with the aim of giving you practical experience with chiral EFT interactions, Bayesian inference, and emulation methods. I will list a few questions during the course, but the most important thing is to run the codes, explore the aspects you find most interesting.

## Python details

There are two directories to pay attention to:
### ./lib

#### the chiral potential

- chiral_potential.py: a class for evaluating the nucleon-nucleon interaction in a two-nucleon partial-wave basis (no isospin breaking or electromagnetic effects) up to next-to-leading order in Weinberg counting. All lecs can be easily changed, and higher orders can be implemented straightforwardly.


```bash
#initialize an object for the chiral interaction (isospin symmetric LO, NLO in WPC available)
potential = chiral_potential.two_nucleon_potential('NLO',Lambda=500.0)
```

#### the nucleon-nucleon analyzer

- nn_studio.py: a class for solving the Lippmann-Schwinger equation for the T-matrix, accessing the two-nucleon potential in selected NN channels, computing phase shifts, and emulating T-matrix amplitudes.

```bash
#initialize an object for computing T-matrices, phase shifts,
nn = nn_studio.nn_studio(jmin=0,jmax=1,tzmin=0,tzmax=0)
# give the potential to the nn-analyzer
nn.V = potential
```
This is the starting point for analyzing the nucleon-nucleon interaction in this course.

### ./examples

The two classes above are imported in all provided examples (./examples):
There are several files with examples (some easier than others). These are meant to serve as a guide for how to use the codes. There is no manual. However, all code is written to be as easy to follow as possible. No pythonic passages or strategies to speedup execution. 
