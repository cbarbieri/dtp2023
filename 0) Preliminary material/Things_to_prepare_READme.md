
# Backgroung material

This folder gathers basic arguments that sould be known by all student by the start of the school. If any of the topics below here is new or unclear to you, make an effort to read the relevant material **before** reaching Trento.


## Basic Quantum Mechanics

To prepare for the Bayesian inference and nuclear forces module you can brush up on angular-momentum coupling and scattering theory. If you can, have a look at Chapters 3.5 (angular momentum) and 6.1 (scattering) in J.J. Sakurai: "Modern Quantum Mechanics" (second edition). See a link [here](https://kgut.ac.ir/useruploads/1505647831850hcd.pdf) for informtion on this book.


## Second quantization and propagators

Many-body perturbation theory and self-consistent Green's function theory are formulated in second quantization (of the Schrödinger's field).

See Chapter 1 of the [intro file](https://gitlab.com/cbarbieri/dtp2023/-/blob/main/0\)%20Preliminary%20material/Intro_to_2nd_quantization_and_propagators.pdf) in this directory to review about Fock space, creation and annihilation operators, and the representation of operators in this framework.

Chapters 2 and 3 gather the basics definitions of many-body Green's function and their links to experimental data.


## Computing

We will have computational hands-on work in the afternoons. Most of this will be based on Python, with some occasional use of c/C++ for some specific applications. Look at the main READme.md file (top directory) for information of the library requirments and so on.

If Phyton or class-based programming (functions, classes, lists, arrays, loops, plotting) are new to you, the following links can serve as good 'get stared' guides:


[https://sites.engineering.ucsb.edu/~shell/che210d/python.pdf](https://sites.engineering.ucsb.edu/~shell/che210d/python.pdf) from UC Santa barbara, is more of a manual.

Chapters 2,3,4,5 of 
[https://cforssen.gitlab.io/tif285-book/content/Intro/welcome.html](https://cforssen.gitlab.io/tif285-book/content/Intro/welcome.html) by C. Forssen. This source also contains some also some material about Bayes and inference.


